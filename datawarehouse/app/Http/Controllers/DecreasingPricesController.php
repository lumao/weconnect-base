<?php

namespace App\Http\Controllers;

use App\Models\DecreasingPrices;
use Illuminate\Http\Request;

class DecreasingPricesController extends Controller
{
    public function saveFromCSV(array $data) {
            foreach($data as $column){
                $codeArticle = $column[0];
                $codeCustomer = $column[1];
                $priceBaseHt = $column[2];
                $tva = $column[3];
                $quantityMax = $column[4];
                $quantityRemaining = $column[5];

                $decreasing = new DecreasingPrices();
                $decreasing->code_article = $codeArticle;
                $decreasing->code_customer = $codeCustomer;
                $decreasing->price_base_ht = $priceBaseHt;
                $decreasing->tva = $tva;
                $decreasing->quantity_max = $quantityMax;
                $decreasing->quantity_remaining = $quantityRemaining;
                $decreasing->save();
        }
    }
}
