<?php

namespace App\Http\Controllers;

use App\Models\Prices;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    public function show()
    {
        return view('import');
    }

    public function importCSV(Request $request)
    {
        /*$request->validate([
            'import_csv' => 'required|file|ends_with:.csv',
        ]);*/
        // Read CSV file and skip the header row
        $entityType = $request->input('entity_type');
        $file = $request->file('import_csv');
        $handle = fopen($file->path(), 'r');

        // Skip the header row
        fgetcsv($handle);

        // Detect delimiter
        $delimiter = $this->detectDelimiter($handle);

        // Define chunk size
        $chunksize = 25;

        // Start reading from the second line
        while (!feof($handle)) {
            $chunkdata = [];
            for ($i = 0; $i < $chunksize; $i++) {
                $data = fgetcsv($handle, 0, $delimiter);
                if ($data === false) {
                    break;
                }
                $chunkdata[] = $data;
            }

            $entity = app("App\\Http\\Controllers\\{$entityType}Controller");
            $entity->saveFromCSV($chunkdata);
        }

        fclose($handle);
    }
    private function detectDelimiter($handle): string
    {
        $line = fgets($handle);
        rewind($handle);
        $semicolon_count = substr_count($line, ';');
        $comma_count = substr_count($line, ',');
        return $semicolon_count > $comma_count ? ';' : ',';
    }
}
