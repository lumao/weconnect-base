<?php

namespace App\Http\Controllers;

use App\Models\PersonalizedPrices;
use Illuminate\Http\Request;

class PersonalizedPricesController extends Controller
{
    public function saveFromCSV(array $data) {
        foreach($data as $column){
            $codeArticle = $column[0];
            $codeCustomer = $column[1];
            $priceBaseHt = $column[2];
            $tva = $column[3];

            $decreasing = new PersonalizedPrices();
            $decreasing->code_article = $codeArticle;
            $decreasing->code_customer = $codeCustomer;
            $decreasing->price_base_ht = $priceBaseHt;
            $decreasing->tva = $tva;
            $decreasing->save();
        }
    }
}
