<?php

namespace App\Http\Controllers;

use App\Models\PersonalizedPrices;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class PricesController extends Controller
{
    public function getPrices(Request $request): JsonResponse
    {
        $this->validate($request, [
            'code_article' => 'required|array',
            'code_article.*' => 'required|string',
        ]);

        $prices = PersonalizedPrices::whereIn('code_article', $request->code_article)->get();
        if ($prices->isEmpty()) {
            return response()->json([
                "message" => "Prices not found for the provided code_articles"
            ], 404);
        }
        return response()->json($prices);
    }

    public function getPrice(Request $request)
    {
        $codeArticle = $request->input('code_article');
        $codeCustomer = $request->input('code_customer');
        $orderedQuantity = $request->input('quantity_ordered', 0);

        // Query to fetch prices from promotion_code table
        $promotionPrices = DB::table('promotion_prices')
            ->where('code_article', $codeArticle)
            ->where('code_customer', $codeCustomer)
            ->whereDate('start_date', '<=', now())
            ->whereDate('end_date', '>=', now())
            ->orderBy('price_base_ht')
            ->get();

        // Query to fetch prices from decreasing_prices table
        $decreasingPrices = DB::table('decreasing_prices')
            ->where('code_article', $codeArticle)
            ->where('code_customer', $codeCustomer)
            ->orderBy('price_base_ht')
            ->get();

        // Combine and process prices from both tables
        $minPrices = [];
        $prevQuantityMax = 0;

        foreach ($promotionPrices as $promotionPrice) {
            $minPrices[] = [
                'price' => $promotionPrice->price_base_ht,
                'quantity_min' => $prevQuantityMax,
                'quantity_max' => $promotionPrice->quantity_remaining + $prevQuantityMax,
                'price_type' => "promotion",
            ];
            $prevQuantityMax = $promotionPrice->quantity_remaining + $prevQuantityMax + 1;
        }

        foreach ($decreasingPrices as $decreasingPrice) {
            $minPrices[] = [
                'price' => $decreasingPrice->price_base_ht,
                'quantity_min' => $prevQuantityMax,
                'quantity_max' => $decreasingPrice->quantity_remaining + $prevQuantityMax,
                'price_type' => "decreasing",
            ];
            $prevQuantityMax = $decreasingPrice->quantity_remaining + $prevQuantityMax + 1;
        }

        // Defined final price based on ordered quantity
        $finalPrice = 0;
        $remainingQuantity = $orderedQuantity;
        foreach ($minPrices as $priceRange) {
            $quantityInRange = min($remainingQuantity, $priceRange['quantity_max'] - $priceRange['quantity_min']);
            $finalPrice += $priceRange['price'] * $quantityInRange;
            $remainingQuantity -= $quantityInRange;
            if ($remainingQuantity <= 0) {
                break;
            }
        }

        // Response
        $response = [
            $codeArticle => [
                'min_prices' => $minPrices,
                'ordered_quantity' => $orderedQuantity,
                'final_price' => $finalPrice,
            ],
        ];

        return response()->json($response);
    }
}