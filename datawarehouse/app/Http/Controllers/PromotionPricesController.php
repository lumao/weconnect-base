<?php

namespace App\Http\Controllers;

use App\Models\PromotionPrices;
use Illuminate\Http\Request;

class PromotionPricesController extends Controller
{
    public function saveFromCSV(array $data) {
        foreach($data as $column){
            $codeArticle = $column[0];
            $codeCustomer = $column[1];
            $priceBaseHt = $column[2];
            $tva = $column[3];
            $startDate = strtotime($column[4]);
            $endDate = strtotime($column[5]);
            $quantityMax = $column[6];
            $quantityRemaining = $column[7];

            $decreasing = new PromotionPrices();
            $decreasing->code_article = $codeArticle;
            $decreasing->code_customer = $codeCustomer;
            $decreasing->price_base_ht = $priceBaseHt;
            $decreasing->tva = $tva;
            $decreasing->start_date = $startDate;
            $decreasing->end_date = $endDate;
            $decreasing->quantity_max = $quantityMax;
            $decreasing->quantity_remaining = $quantityRemaining;
            $decreasing->save();
        }
    }
}
