<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    use HasFactory;
    protected $fillable = [
        'code_article',
        'code_customer',
        'price_base_ht',
        'tva',
        'quantity_max',
        'quantity_remaining',
        'start_date',
        'end_date',
        'business_name',
        'business_code',
    ];
}
