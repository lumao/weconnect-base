<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DecreasingPrices extends Model
{
    use HasFactory;
    protected $fillable = [
        'code_article',
        'code_customer',
        'price_base_ht',
        'tva',
        'start_date',
        'end_date',
        'quantity_max',
        'quantity_remaining',
    ];
}