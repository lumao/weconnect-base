<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalizedPrices extends Model
{
    use HasFactory;
    protected $fillable = [
        'code_article',
        'code_customer',
        'code_',
        'price_base_ht',
        'tva',
    ];
}