<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prices extends Model
{
    use HasFactory;
    protected $fillable = [
        'code_article',
        'code_tier',
        'code_promo',
        'price_base_ht',
        'price_tier_ht',
        'sorecop_ht',
        'ecopart_ht',
        'tva',
        'quantity_max_customer',
        'quantity_max',
        'quantity_min',
        'quantity_sold',
        'quantity_free',
    ];
}
