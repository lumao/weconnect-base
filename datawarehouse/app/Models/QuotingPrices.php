<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotingPrices extends Model
{
    use HasFactory;
    protected $fillable = [
        'code_article',
        'code_customer',
        'price_base_ht',
        'tva',
        'quantity_max',
        'quantity_remaining',
        'business_name',
        'business_code',
    ];
}
