<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PricesFactory extends Factory
{

    public function definition(): array
    {
        return [
            'code_article' => fake()->regexify('[A-Za-z0-9]{15}'),
            'code_tier' => fake()->regexify('[A-Za-z0-9]{15}'),
            'code_promo' => fake()->regexify('[A-Za-z0-9]{15}'),
            'price_base_ht' => fake()->randomFloat(2,0,2),
            'price_tier_ht' => fake()->randomFloat(2,0,2),
            'sorecop_ht' => fake()->randomFloat(2,0,2),
            'ecopart_ht' => fake()->randomFloat(2,0,2),
            'tva' => 20,
            'quantity_max_customer' => fake()->numberBetween(0,100000),
            'quantity_max' => fake()->numberBetween(0,100000),
            'quantity_min' => fake()->numberBetween(0,100000),
            'quantity_sold' => fake()->numberBetween(0,100000),
            'quantity_free' => fake()->numberBetween(0,100000),
        ];
    }
}
