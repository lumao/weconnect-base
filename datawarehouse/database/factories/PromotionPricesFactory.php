<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PromotionPrices>
 */
class PromotionPricesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'code_article' => fake()->regexify('[A-Za-z0-9]{15}'),
            'code_customer' => fake()->regexify('[A-Za-z0-9]{15}'),
            'price_base_ht' => fake()->randomFloat(2,0,2),
            'tva' => 20,
            'start_date' => fake()->dateTime(),
            'end_date' => fake()->dateTime(),
            'quantity_max' => fake()->numberBetween(0,100000),
            'quantity_remaining' => fake()->numberBetween(0,100000),
        ];
    }
}
