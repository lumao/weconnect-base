<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->string('code_article');
            $table->string('code_tier');
            $table->string('code_promo');
            $table->float('price_base_ht');
            $table->float('price_tier_ht');
            $table->float('sorecop_ht');
            $table->float('ecopart_ht');
            $table->float('tva');
            $table->integer('quantity_max_customer');
            $table->integer('quantity_max');
            $table->integer('quantity_min');
            $table->integer('quantity_sold');
            $table->integer('quantity_free');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prices');
    }
};
