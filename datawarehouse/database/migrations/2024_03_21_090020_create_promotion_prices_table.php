<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('promotion_prices', function (Blueprint $table) {
            $table->id();
            $table->string('code_article');
            $table->string('code_customer');
            $table->float('price_base_ht');
            $table->float('tva');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('quantity_max');
            $table->integer('quantity_remaining');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('promotion_prices');
    }
};
