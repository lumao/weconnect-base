<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('quoting_prices', function (Blueprint $table) {
            $table->id();
            $table->string('code_article');
            $table->string('code_customer');
            $table->float('price_base_ht');
            $table->float('tva');
            $table->integer('quantity_max');
            $table->integer('quantity_remaining');
            $table->string('business_name');
            $table->string('business_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('quoting_prices');
    }
};
