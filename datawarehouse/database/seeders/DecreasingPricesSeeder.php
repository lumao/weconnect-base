<?php

namespace Database\Seeders;

use App\Models\DecreasingPrices;
use Illuminate\Database\Seeder;

class DecreasingPricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DecreasingPrices::factory()->count(200)->create();
    }
}
