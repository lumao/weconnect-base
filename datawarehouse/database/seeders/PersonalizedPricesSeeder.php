<?php

namespace Database\Seeders;

use App\Models\PersonalizedPrices;
use Illuminate\Database\Seeder;

class PersonalizedPricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PersonalizedPrices::factory()->count(200)->create();
    }
}
