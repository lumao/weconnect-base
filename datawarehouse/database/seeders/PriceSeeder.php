<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Prices;

class PriceSeeder extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 5000; $i++) {
            Prices::factory()->count(200)->create();
        }
    }
}