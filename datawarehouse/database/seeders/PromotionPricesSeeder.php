<?php

namespace Database\Seeders;

use App\Models\PromotionPrices;
use Illuminate\Database\Seeder;

class PromotionPricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PromotionPrices::factory()->count(200)->create();
    }
}
