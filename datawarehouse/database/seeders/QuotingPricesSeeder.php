<?php

namespace Database\Seeders;

use App\Models\QuotingPrices;
use Illuminate\Database\Seeder;

class QuotingPricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        QuotingPrices::factory()->count(200)->create();
    }
}
