<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <title>Document</title>
    <style>
        /* Your custom CSS styles go here */
    </style>
</head>
<body>
<div class="container">
    <div>
        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="messages">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
            <div class="fields">
                <div class="input-group mb-3">
                    <input type="file" class="form-control" id="import_csv" name="import_csv" accept=".csv">
                    <label class="input-group-text" for="import_csv">Upload</label>
                </div>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" value="DecreasingPrices" id="entity_decreasingPrice" name="entity_type">
                <label class="form-check-label" for="entity_decreasingPrice">
                    Decreasing Price
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" value="PersonalizedPrices" id="entity_personalizedPrice" name="entity_type">
                <label class="form-check-label" for="entity_personalizedPrice">
                    Personalized Price
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" value="PromotionPrices" id="entity_promotionPrice" name="entity_type">
                <label class="form-check-label" for="entity_promotionPrice">
                    Promotion Price
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" value="QuotingPrices" id="entity_quotingPrice" name="entity_type">
                <label class="form-check-label" for="entity_quotingPrice">
                    Quoting Price
                </label>
            </div>
            <button type="submit" class="btn btn-success">Import CSV</button>
        </form>
    </div>
</div>
</body>
</html>